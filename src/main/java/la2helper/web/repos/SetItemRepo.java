package la2helper.web.repos;

import la2helper.web.domain.SetItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SetItemRepo extends JpaRepository<SetItem, Long> {
    List<SetItem> findAll();
}