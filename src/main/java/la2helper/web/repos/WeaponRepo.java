package la2helper.web.repos;

import la2helper.web.domain.Weapon;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WeaponRepo extends JpaRepository<Weapon, Long> {
    List<Weapon> findAll();
}