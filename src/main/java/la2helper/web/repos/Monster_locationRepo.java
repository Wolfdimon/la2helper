package la2helper.web.repos;

import la2helper.web.domain.Monster_location;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface Monster_locationRepo extends JpaRepository<Monster_location, Long> {
    List<Monster_location> findAll();
}
