package la2helper.web.repos;

import la2helper.web.domain.Drop;
import la2helper.web.domain.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DropRepo extends JpaRepository<Drop, Long> {
    List<Drop> findAll();
}