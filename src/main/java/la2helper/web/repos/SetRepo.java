package la2helper.web.repos;

import la2helper.web.domain.Set;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SetRepo extends JpaRepository<Set, Long> {
    List<Set> findAll();
}