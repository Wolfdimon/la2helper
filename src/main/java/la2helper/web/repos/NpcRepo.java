package la2helper.web.repos;

import la2helper.web.domain.Npc;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NpcRepo extends JpaRepository<Npc, Long> {
    List<Npc> findAll();
}