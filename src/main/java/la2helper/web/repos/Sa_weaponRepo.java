package la2helper.web.repos;

import la2helper.web.domain.Sa_weapon;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface Sa_weaponRepo extends JpaRepository<Sa_weapon, Long> {
    List<Sa_weapon> findAll();
}