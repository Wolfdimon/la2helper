package la2helper.web.repos;

import la2helper.web.domain.Itemname;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ItemnameRepo extends JpaRepository<Itemname, Long> {
    List<Itemname> findAll();
}