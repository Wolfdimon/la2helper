package la2helper.web.repos;

import la2helper.web.domain.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IngredientRepo extends JpaRepository<Ingredient, Long> {
    List<Ingredient> findAll();
}