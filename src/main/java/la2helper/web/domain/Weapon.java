package la2helper.web.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Weapon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer hands;
    private Integer soulshot_count;
    private String crystal_type;
    private Integer crystal_count;
    private Integer physical_damage;
    private String weapon_type;
    private Integer magical_damage;
    private Integer speed;
    private Integer critical;

    public Weapon() {
    }

    public Integer getHands() {
        return hands;
    }

    public void setHands(Integer hands) {
        this.hands = hands;
    }

    public Integer getSoulshot_count() {
        return soulshot_count;
    }

    public void setSoulshot_count(Integer soulshot_count) {
        this.soulshot_count = soulshot_count;
    }

    public String getCrystal_type() {
        return crystal_type;
    }

    public void setCrystal_type(String crystal_type) {
        this.crystal_type = crystal_type;
    }

    public Integer getCrystal_count() {
        return crystal_count;
    }

    public void setCrystal_count(Integer crystal_count) {
        this.crystal_count = crystal_count;
    }

    public Integer getPhysical_damage() {
        return physical_damage;
    }

    public void setPhysical_damage(Integer physical_damage) {
        this.physical_damage = physical_damage;
    }

    public String getWeapon_type() {
        return weapon_type;
    }

    public void setWeapon_type(String weapon_type) {
        this.weapon_type = weapon_type;
    }

    public Integer getMagical_damage() {
        return magical_damage;
    }

    public void setMagical_damage(Integer magical_damage) {
        this.magical_damage = magical_damage;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public Integer getCritical() {
        return critical;
    }

    public void setCritical(Integer critical) {
        this.critical = critical;
    }
}
