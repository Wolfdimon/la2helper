package la2helper.web.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Npc {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String title;
    private String name;
    private String level;
    private String org_hp;
    private Integer exp;
    private Integer sp;
    private Integer phys_attack;
    private Integer magic_attack;
    private Integer attack_range;
    private Integer run_speed;
    private Boolean npc;
    private String agro;
    private String category;

    private String weakness;

    public Npc() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getOrg_hp() {
        return org_hp;
    }

    public void setOrg_hp(String org_hp) {
        this.org_hp = org_hp;
    }

    public Integer getExp() {
        return exp;
    }

    public void setExp(Integer exp) {
        this.exp = exp;
    }

    public Integer getSp() {
        return sp;
    }

    public void setSp(Integer sp) {
        this.sp = sp;
    }

    public Integer getPhys_attack() {
        return phys_attack;
    }

    public void setPhys_attack(Integer phys_attack) {
        this.phys_attack = phys_attack;
    }

    public Integer getMagic_attack() {
        return magic_attack;
    }

    public void setMagic_attack(Integer magic_attack) {
        this.magic_attack = magic_attack;
    }

    public Integer getAttack_range() {
        return attack_range;
    }

    public void setAttack_range(Integer attack_range) {
        this.attack_range = attack_range;
    }

    public Integer getRun_speed() {
        return run_speed;
    }

    public void setRun_speed(Integer run_speed) {
        this.run_speed = run_speed;
    }

    public Boolean getNpc() {
        return npc;
    }

    public void setNpc(Boolean npc) {
        this.npc = npc;
    }

    public String getAgro() {
        return agro;
    }

    public void setAgro(String agro) {
        this.agro = agro;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getWeakness() {
        return weakness;
    }

    public void setWeakness(String weakness) {
        this.weakness = weakness;
    }
}
