package la2helper.web.domain;

import javax.persistence.*;

@Entity
public class Monster_location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer x;
    private Integer y;
    private Integer z;
    private Boolean main_location;

    @ManyToOne(fetch = FetchType.LAZY)
    private Npc npc;

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Integer getZ() {
        return z;
    }

    public void setZ(Integer z) {
        this.z = z;
    }

    public Boolean getMain_location() {
        return main_location;
    }

    public void setMain_location(Boolean main_location) {
        this.main_location = main_location;
    }

    public Npc getNpc() {
        return npc;
    }

    public void setNpc(Npc npc) {
        this.npc = npc;
    }

    public Monster_location() {
    }
}
