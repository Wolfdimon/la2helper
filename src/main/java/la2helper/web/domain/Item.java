package la2helper.web.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Items")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Integer id;
    private String name_item;
    private String type;
    private String material_type;
    private String icon;
    private Integer weight;
    private String slot_bit_type;

    @OneToMany(mappedBy = "item", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Recipe> recipes;

/*    @CollectionTable(name = "itemnames", joinColumns = @JoinColumn(name = "id"))
    private Itemname itemname;*/

    public Item() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }

    public String getName() {
        return name_item;
    }

    public void setName(String name) {
        this.name_item = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMaterial_type() {
        return material_type;
    }

    public void setMaterial_type(String material_type) {
        this.material_type = material_type;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getSlot_bit_type() {
        return slot_bit_type;
    }

    public void setSlot_bit_type(String slot_bit_type) {
        this.slot_bit_type = slot_bit_type;
    }
}
