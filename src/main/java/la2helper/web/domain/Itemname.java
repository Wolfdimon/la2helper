package la2helper.web.domain;

import javax.persistence.*;

@Entity
@Table(name = "Itemnames")
public class Itemname extends Item{
/*    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;*/

    private String name;
    private String description;

    public Itemname() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
