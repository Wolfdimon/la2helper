package la2helper.web.domain;

import javax.persistence.*;

@Entity
public class SetItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Set set;

    @ManyToOne(fetch = FetchType.LAZY)
    private Item item;

    public SetItem() {
    }

    public Set getSet() {
        return set;
    }

    public void setSet(Set set) {
        this.set = set;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
