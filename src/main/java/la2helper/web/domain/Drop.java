package la2helper.web.domain;

import javax.persistence.*;

@Entity
public class Drop {
    //Здесь был я. А теперь я ушел от сюда
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer min;
    private Integer max;
    private Integer sweep;
    private String percentage;

    @ManyToOne(fetch = FetchType.LAZY)
    private Npc npc;

    @ManyToOne(fetch = FetchType.LAZY)
    private Item item;

    public Drop() {
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Integer getSweep() {
        return sweep;
    }

    public void setSweep(Integer sweep) {
        this.sweep = sweep;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public Npc getNpc() {
        return npc;
    }

    public void setNpc(Npc npc) {
        this.npc = npc;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
