package la2helper.web.domain;

import javax.persistence.*;

@Entity
public class Recipe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Item item;

    private Integer level;
    private Integer mp;
    private Integer output;
    private String npc_fee;

    public Recipe() {
    }

    public Integer id() {
        return id;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getMp() {
        return mp;
    }

    public void setMp(Integer mp) {
        this.mp = mp;
    }

    public Integer getOutput() {
        return output;
    }

    public void setOutput(Integer output) {
        this.output = output;
    }

    public String getNpc_fee() {
        return npc_fee;
    }

    public void setNpc_fee(String npc_fee) {
        this.npc_fee = npc_fee;
    }
}
