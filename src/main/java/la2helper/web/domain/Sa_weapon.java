package la2helper.web.domain;

import javax.persistence.*;

@Entity
public class Sa_weapon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Item item;

//    @ManyToOne(fetch = FetchType.LAZY)
//    private Ingredient ingredient;

    private Integer quantity;
}
