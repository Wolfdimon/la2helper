package la2helper.web.controller;

import la2helper.web.domain.Item;
import la2helper.web.domain.Itemname;
import la2helper.web.domain.Recipe;
import la2helper.web.domain.User;
import la2helper.web.repos.ItemRepo;
import la2helper.web.repos.ItemnameRepo;
import la2helper.web.repos.RecipeRepo;
import la2helper.web.repos.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.Map;

@Controller
public class Maincontroller {
    @Autowired
    private RecipeRepo recipeRepo;
    @Autowired
    private ItemRepo itemRepo;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    private ItemnameRepo itemnameRepo;

    @GetMapping("/")
    public String home(Map<String, Object> model) {
        Iterable<Recipe> recipes = recipeRepo.findAll();
        model.put("recipes", recipes);

        return "main";
    }

    @GetMapping("/login")
    public String login(Map<String, Object> model) {

        return "login";
    }

    @GetMapping("/lk")
    public String glk(User user, Map<String, Object> model) {
        //if (userFromDb != null) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        //User userFromDb = userRepo.findByUsername(name);
        model.put("users", name);
       // List<Item> items = itemRepo.findAll();
       // model.put("items", items);
        List<Itemname> itemnames = itemnameRepo.findAll();
        model.put("items", itemnames);
        return "lk";
    }

    @PostMapping("/lk")
    public String plk(Map<String, Object> model) {

        return "lk";
    }
}
